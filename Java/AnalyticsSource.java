package analytics;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/*dependencies {
    implementation 'org.apache.poi:poi:3.8'
}*/

public class AnalyticsSource {
    private static List<String> dirList = new ArrayList<>();
    private static List<String> fileList = new ArrayList<>();
    private static List<String> nameList = new ArrayList<>();
    private static List<String> lineList = new ArrayList<>();
    private static List<String> sourcesList = new ArrayList<>();
    private static List<String> allList = new ArrayList<>();
    private static int sumLine = 0;
//    public static String dirPathInput = "D:\\06code\\19Tools\\Java\\ToolsJava\\src\\main\\java";
//    public static String replacePath  = "D:\\06code\\19Tools\\Java\\ToolsJava\\src\\main";
    public static String dirPathInput = "D:\\06code\\00AnalyticsSources\\Sources\\01OkHttpSources\\okhttp-3.10.0-sources\\okhttp3";
    public static String replacePath  = "D:\\06code\\00AnalyticsSources\\Sources\\01OkHttpSources\\okhttp-3.10.0-sources\\";
    public static void main(String[] args) {
        analyticsDir(dirPathInput);
        addAll(replacePath);
        printSourcesMessage();
        createExcel(replacePath);
    }

    private static void createExcel(String replace) {
        try {
            File file = new File("D:\\06code\\00AnalyticsSources\\Analytics\\analytics.xls");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheetRDQ = workbook.createSheet("SheetRDQ");
            sheetRDQ.setColumnWidth(0,6000);
            sheetRDQ.setColumnWidth(1,12000);
            sheetRDQ.setColumnWidth(2,12000);
            sheetRDQ.setColumnWidth(3,6000);
            for (int i = 0; i < fileList.size() + 2; i++) {
                HSSFRow row = sheetRDQ.createRow(i);
                row.createCell(0);
                row.createCell(1);
                row.createCell(2);
                row.createCell(3);
                row.setHeightInPoints(30);
            }
            HSSFRow row = sheetRDQ.getRow(0);
            HSSFCell cell = row.getCell(0);
            sheetRDQ.getRow(0).getCell(0).setCellValue("Dir  number : " + dirList.size());
            sheetRDQ.getRow(0).getCell(1).setCellValue("File number : " + fileList.size());
            sheetRDQ.getRow(0).getCell(2).setCellValue("Name number : " + nameList.size());
            sheetRDQ.getRow(0).getCell(3).setCellValue("Sum Line : " + sumLine);

            for (int i = 0; i < dirList.size(); i++) {
                String s = dirList.get(i);
                s = s.replace(replace, "");
                sheetRDQ.getRow(i + 1).getCell(0).setCellValue(s);
            }
            for (int i = 0; i < fileList.size(); i++) {
                String s = fileList.get(i);
                s = s.replace(replace, "");
                sheetRDQ.getRow(i + 1).getCell(1).setCellValue(s);
            }
            for (int i = 0; i < nameList.size(); i++) {
                String s = nameList.get(i);
                s = s.replace(replace, "");
                sheetRDQ.getRow(i + 1).getCell(2).setCellValue(s);
            }
            for (int i = 0; i < lineList.size(); i++) {
                String s = lineList.get(i);
                s = s.replace(replace, "");
                sheetRDQ.getRow(i + 1).getCell(3).setCellValue(s);
            }
            workbook.write(fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            System.out.println("write failed");
        }
    }


    private static void printSourcesMessage() {
        try {
            File file = new File("D:\\06code\\00AnalyticsSources\\Analytics\\analytics.txt");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(fileOutputStream);
            for (String s : allList) {
                writer.write(s + "\n");
            }
            writer.close();
            fileOutputStream.close();
            System.out.println("write success");
        } catch (IOException e) {
            System.out.println("write failed");
        }
    }

    private static void addAll(String replace) {
        allList.add("Dir  number : " + dirList.size());
        for (String s : dirList) {
            s = s.replace(replace, "");
            allList.add(s);
        }
        allList.add("File number : " + fileList.size());
        for (String s : fileList) {
            s = s.replace(replace, "");
            allList.add(s);
        }
        allList.add("Name number : " + nameList.size());
        for (String s : nameList) {
            allList.add(s);
        }
        allList.add("Sum Line : " + sumLine);
        for (String s : lineList) {
            allList.add(s);
        }
        allList.add("Sources sum : " + sumLine);
        for (String s : sourcesList) {
            allList.add(s);
        }
    }


    private static void analyticsDir(String dirPath) {
        if (dirPath == null || dirPath.length() == 0) {
            System.out.println("dirPath is empty.");
            return;
        }
        File file = new File(dirPath);
        if (!file.exists()) {
            System.out.println("dir not exist.");
            return;
        }
        String absolutePath = file.getAbsolutePath();
        if (file.isFile()) {
            fileList.add(absolutePath);
            return;
        }
        if (file.isDirectory()) {
            dirList.add(dirPath);
            String[] list = file.list();
            for (String sonDir : list) {
                sonDir = dirPath + "/" + sonDir;
                File sonFile = new File(sonDir);
                if (sonFile.isDirectory()) {
                    analyticsDir(sonDir);
                } else {
                    try {
                        String sonAbsolutePath = sonFile.getAbsolutePath();
                        LineNumberReader reader = new LineNumberReader(new FileReader(sonAbsolutePath));
                        reader.skip(Long.MAX_VALUE);
                        int lineNumber = reader.getLineNumber();
                        lineList.add(lineNumber + "");
                        sumLine += lineNumber;

                        fileList.add(sonAbsolutePath + " " + lineNumber);
                        String sonFileName = sonFile.getName();
                        nameList.add(sonFileName + " " + lineNumber);

                        byte[] bytes = Files.readAllBytes(Paths.get(sonAbsolutePath));
                        String source = new String(bytes, StandardCharsets.UTF_8);
                        sourcesList.add("Current class is : " + sonFileName + " lines " + lineNumber);
                        sourcesList.add(source);
                    } catch (IOException e) {
                        System.out.println(e.toString());
                    }
                }
            }
        }

    }
}
